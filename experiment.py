# -*- coding: utf-8 -*-

"""
This code is a one-time application of the Eulerian Video Magnification algorithm, presented here:
http://people.csail.mit.edu/mrub/evm/

The file used for the initial analysis is face.mp4, which is included in this directory

The version of openCV used in this file is 2.4.13
"""


# Import libraries
import cv2
import numpy as np

def extract_frames(video_filename):
    """
    The following function takes the input video and splits it into its individual frames
    Each of these frames are then placed into an array. If the video has 3 color channels, then
    the array dimensions are N x H x W x 3, where H is the number of pixels in the height of the video,
    W is the number of pixels in the width, 3 is the number of color channels, and N is the number of frames
    """
    video = cv2.VideoCapture(video_filename)
    # Initiailize a list
    image_array = []
    
    while True:
        success,image = video.read()
        if not success:
            break
        image_array.append(image)
    
    return np.asarray(image_array)


def extract_fps(video_filename):
    """ 
    This function is just a wrapper around opencv functions to return the fps as a float (default)
    """
    video = cv2.VideoCapture(video_filename)
    return video.get(cv2.cv.CV_CAP_PROP_FPS)
    
def create_laplacian_array(image_array, num_layers):
    """
    This function takes in an image array as input and returns the corresponding arrays of 
    laplacians for the number of layers specified. The lowest granularity layer will be the base image.
    Another function will be used to build up the changed images.
    
    Written as nested for loops for now, but may be able to add a vectorization layer using numpy TODO
    """
    return_dict = {}
    """
    The return dictionary will have the following properties:
    The key is the frame number in question
    Each value will be a list with the image matrix from smallest to largest (base Gaussian)
    For example, for a 592 by 528 image, and 4 total layers of laplacians, the corresponding dimensions would be
    74x66 (base gaussian), 148x132, 296x264, 592x528
    Technically, I think this deviates from the canonical definition of the number of layers (the above would be a 3-layer laplacian), but for clarity
    in terms of the data structure, I'll define it this way
    """
    for frame_n in range(0, image_array.shape[0]):
        # Here, frame_n is the frame number
        
        
        # Next, this part creates a laplacian
        gaussians = [image_array[frame_n].copy()]
        for layer in range(1, num_layers):
            gaussians.append(cv2.pyrDown(gaussians[layer-1]))
            
        laplacians = []
        laplacians.append(gaussians[-1])
        
        for index in range(num_layers-1, 0, -1):
            reconstructed_gaussian = cv2.pyrUp(gaussians[index])
            l = cv2.subtract(gaussians[index-1], reconstructed_gaussian)
            laplacians.append(l)
        
        return_dict[frame_n] = laplacians
    
    return return_dict


def temporal_bandpass_filter(signal, fps, freq_min=0.833, freq_max = 1.0, amplification_factor=1):
    """ 
    The following performs a temporal_bandpass_filter and returns the original signal with the amplified frequency ranges
    """
    
    fftd = np.fft.fft(signal)
    frequencies = np.fft.fftfreq(len(signal), d = 1.0/fps)
    mask = [amplification_factor  if freq_min <= np.abs(n) <= freq_max else 0 for n in frequencies]
    
    amplified = mask * fftd + fftd
    
    result = np.abs(np.fft.ifft(amplified))
    
    return result


def build_video_from_laplacian(laplacian_dictionary, fps, save_filename):
    """
    This function undoes the laplacian pyramid construction
    and returns the video
    """
    result_array = []
    for n in laplacian_dictionary:
        # Collapse the elements
        img = laplacian_dictionary[n][0]
        for i in range(0, len(laplacian_dictionary[n])-1):
            img = cv2.pyrUp(img) + laplacian_dictionary[n][i+1]
        
        result_array.append(img)
        print(len(result_array))
    
    # Build video
    height, width, depth = result_array[0].shape
    writer = cv2.VideoWriter(save_filename, cv2.cv.CV_FOURCC('M', 'P', 'E', 'G'), fps, (width, height))
    for i in range(0, len(result_array)):
        writer.write(result_array[i])
    
    cv2.destroyAllWindows()
    writer.release()
    pass

def amplify_video(laplacian_dictionary, fps, freq_min, freq_max, amplification_factor):
    """
    This function will pass each pixel signal from the laplacian dictionary structure
    through a bandpass filter and eventually return the updated laplacian dictionary
    """
    laplacian = laplacian_dictionary.copy()
    for pyr_index in range(0, len(laplacian_dictionary[0])):
        for height in range(0, np.shape(laplacian_dictionary[0][pyr_index])[0]):
            for width in range(0, np.shape(laplacian_dictionary[0][pyr_index])[1]):
                for channel in range(0, 3):
                    pixels = [laplacian_dictionary[n][pyr_index][height][width][channel] for n in laplacian_dictionary]
                    augmented = temporal_bandpass_filter(pixels, fps, freq_min, freq_max, amplification_factor)
                    for n in laplacian_dictionary:
                        laplacian[n][pyr_index][height][width][channel] = augmented[n]
    return laplacian





if __name__ == '__main__':
    
    # First, read in the video file using opencv video capture
    video_filename = "face.mp4"
    video = cv2.VideoCapture(video_filename)
    
    # How many FPS is this video?
    print("The number of frames per second in this video is %d" % video.get(cv2.cv.CV_CAP_PROP_FPS))
    
    image_array = extract_frames(video_filename)
    
    laplacians = create_laplacian_array(image_array, 4)
    
    # Track a pixel over time
#    import matplotlib.pyplot as plt
#    test_pixel = [laplacians[n][1][80][80][0] for n in laplacians]
#    fftd = np.fft.fft(test_pixel)
#    frequencies = np.fft.fftfreq(301)*30
#    psd = np.abs(fftd)**2
    laplacian_amplified = amplify_video(laplacians, 30, .83333, 1.0, 100)
    
    build_video_from_laplacian(laplacian_amplified, 30, "test.mpg")
